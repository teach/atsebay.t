package main

import (
	"context"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type App struct {
	router *gin.Engine
	srv    *http.Server
}

func NewApp() App {
	gin.ForceConsoleColor()
	router := gin.Default()

	declareStaticRoutes(router)
	declareAPIRoutes(router)

	app := App{
		router: router,
	}

	return app
}

func (app *App) Start(bind string) {
	app.srv = &http.Server{
		Addr:    bind,
		Handler: app.router,
	}

	if err := app.srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatalf("listen: %s\n", err)
	}
}

func (app *App) Stop() {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := app.srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}
}
