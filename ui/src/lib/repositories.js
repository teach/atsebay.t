export class WorkRepository {
  constructor(res) {
    if (res) {
      this.update(res);
    }
  }

  update({ id, id_user, id_work, uri, secret, last_check, already_used }) {
    this.id = id;
    this.id_user = id_user;
    this.id_work = id_work;
    this.uri = uri;
    this.secret = secret;
    this.last_check = last_check;
    this.already_used = already_used == true;
  }

  async delete(userid) {
    let url = this.id_work?`works/${this.id_work}/repositories/${this.id}`:`repositories/${this.id}`;

    if (userid) {
      url = `users/${userid}/` + url;
    }

    const res = await fetch("api/" + url, {
      method: 'DELETE',
      headers: {'Accept': 'application/json'}
    });
    if (res.status == 200) {
      return true;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async getBuildState() {
    const res = await fetch(this.id_work?`api/works/${this.id_work}/repositories/${this.id}/state`:`api/repositories/${this.id}/state`, {
      headers: {'Accept': 'application/json'}
    });
    if (res.status == 200) {
      return await res.json();
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async getBuildLogs(userid) {
    let url = this.id_work?`works/${this.id_work}/repositories/${this.id}/state-logs`:`repositories/${this.id}/state-logs`;

    if (userid) {
      url = `users/${userid}/` + url;
    }

    const res = await fetch("api/" + url, {
      headers: {'Accept': 'application/json'}
    });
    if (res.status == 200) {
      return await res.json();
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async retrieveWork(admin_struct) {
    const res = await fetch(this.id_work?`api/works/${this.id_work}/repositories/${this.id}/trigger`:`api/repositories/${this.id}/trigger`, {
      method: 'POST',
      headers: {'Accept': 'application/json'},
      body: !admin_struct?{}:JSON.stringify(admin_struct)
    });
    if (res.status == 200) {
      const data = await res.json();
      this.update(data);
      return data;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async runGradation() {
    const res = await fetch(this.id_work?`api/works/${this.id_work}/repositories/${this.id}/gradation`:`api/repositories/${this.id}/gradation`, {
      method: 'POST',
      headers: {'Accept': 'application/json'},
    });
    if (res.status == 200) {
      return await res.json();
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async gradationStatus() {
    const res = await fetch(this.id_work?`api/works/${this.id_work}/repositories/${this.id}/gradation_status`:`api/repositories/${this.id}/gradation_status`, {
      method: 'GET',
      headers: {'Accept': 'application/json'},
    });
    if (res.status == 200) {
      return await res.json();
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async save(user) {
    let url = this.id?`repositories/${this.id}`:'repositories';

    if (this.id_work) {
      url = `works/${this.id_work}/` + url;
    }
    if (user != null) {
      url = `users/${user.id}/` + url;
    }

    const res = await fetch("api/"+url, {
      method: this.id?'PUT':'POST',
      headers: {'Accept': 'application/json'},
      body: JSON.stringify(this),
    });
    if (res.status == 200) {
      const data = await res.json();
      this.update(data);
      return data;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }
}

export async function getRemoteRepositories(userid) {
  const res = await fetch(userid?`api/users/${userid}/gitlabcri/repositories`:`api/gitlabcri/repositories`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return await res.json();
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getRepositories(wid, userid) {
  const res = await fetch(userid?`api/users/${userid}/works/${wid}/repositories`:`api/works/${wid}/repositories`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    const data = await res.json()
    if (data) {
      return data.map((r) => new WorkRepository(r));
    } else {
      throw new Error("No repository attached");
    }
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getRepository(kid) {
  const res = await fetch(`api/repositories/${kid}`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return new Repository(await res.json());
  } else {
    throw new Error((await res.json()).errmsg);
  }
}
