export async function getPromos() {
  const res = await fetch('api/promos', {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return await res.json();
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getUsers(promo, group) {
  let url = '/api/users?';
  if (promo) url += "promo=" + encodeURIComponent(promo) + "&";
  if (group) url += "group=" + encodeURIComponent(group) + "&";
  const res = await fetch(url, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return await res.json();
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function anonOldAccounts() {
  const res = await fetch('api/users', {
    method: 'PATCH',
    headers: {'Accept': 'application/json'},
  });
  if (res.status == 200) {
    return await res.json()
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export class User {
  constructor(res) {
    if (res) {
      this.update(res);
    }
  }

  update({ id, login, email, firstname, lastname, time, promo, groups, is_admin }) {
    this.id = id;
    this.login = login;
    this.email = email;
    this.firstname = firstname;
    this.lastname = lastname;
    this.time = time;
    this.promo = promo;
    this.groups = groups;
    this.is_admin = is_admin;
  }

  async save() {
    const res = await fetch(this.id?`api/users/${this.id}`:'api/users', {
      method: this.id?'PUT':'POST',
      headers: {'Accept': 'application/json'},
      body: JSON.stringify(this),
    });
    if (res.status == 200) {
      const data = await res.json()
      this.update(data);
      return data;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }
}

export async function getUser(uid) {
  const res = await fetch(`api/users/${uid}`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return new User(await res.json());
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getGrades(uid, survey) {
  const res = await fetch(`api/grades`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return await res.json();
  } else {
    throw new Error((await res.json()).errmsg);
  }
}
export async function getUserGrade(uid, survey) {
  const res = await fetch(`api/users/${uid}/surveys/${survey.id}/grades`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return await res.json();
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getUserScore(uid, survey) {
  const res = await fetch(`api/users/${uid}/surveys/${survey.id}/score`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return await res.json();
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export class UserNeedingHelp {
  constructor(res) {
    if (res) {
      this.update(res);
    }
  }

  update({ id, id_user, date, comment, treated }) {
    this.id = id;
    this.id_user = id_user;
    this.date = new Date(date);
    this.comment = comment;
    if (treated) {
      this.treated = new Date(treated);
    } else {
      this.treated = null;
    }
  }

  mark_treated() {
    this.treated = new Date();
  }

  async save() {
    const res = await fetch(this.id?`api/help/${this.id}`:'api/help', {
      method: this.id?'PUT':'POST',
      headers: {'Accept': 'application/json'},
      body: JSON.stringify(this),
    });
    if (res.status == 200) {
      const data = await res.json()
      this.update(data);
      return data;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }
}

export async function getUserNeedingHelp() {
  const res = await fetch(`api/help`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return (await res.json()).map((nh) => {
      return new UserNeedingHelp(nh)
    });
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getScore(survey) {
  const res = await fetch(survey.kind === "w" ? `api/works/${survey.id}/score` : `api/surveys/${survey.id}/score`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return await res.json();
  } else {
    throw new Error((await res.json()).errmsg);
  }
}
