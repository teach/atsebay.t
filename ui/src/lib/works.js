import { Grade } from '$lib/grades';

export class Work {
  constructor(res) {
    this.kind = "w";
    if (res) {
      this.update(res);
    }
  }

  update({ id, id_category, title, promo, group, shown, tag, description, descr_raw, submission_url, gradation_repo, corrected, start_availability, end_availability }) {
    this.id = id;
    this.id_category = id_category;
    this.title = title;
    this.promo = promo;
    this.group = group;
    this.shown = shown;
    this.tag = tag;
    this.description = description;
    this.descr_raw = descr_raw;
    this.submission_url = submission_url;
    this.gradation_repo = gradation_repo;
    this.corrected = corrected;
    if (this.start_availability != start_availability) {
      this.start_availability = start_availability;
      delete this.__start_availability;
    }
    if (this.end_availability != end_availability) {
      this.end_availability = end_availability;
      delete this.__end_availability;
    }
  }

  startAvailability() {
    if (!this.__start_availability) {
      this.__start_availability = new Date(this.start_availability)
    }
    return this.__start_availability
  }

  endAvailability() {
    if (!this.__end_availability) {
      this.__end_availability = new Date(this.end_availability)
    }
    return this.__end_availability
  }

  isFinished() {
    return this.endAvailability() < new Date();
  }

  async save() {
    const res = await fetch(this.id?`api/works/${this.id}`:'api/works', {
      method: this.id?'PUT':'POST',
      headers: {'Accept': 'application/json'},
      body: JSON.stringify(this),
    });
    if (res.status == 200) {
      const data = await res.json()
      this.update(data);
      return data;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async duplicate() {
    if (this.id) {
      const oldSurveyId = this.id;
      delete this.id;
      const res = await fetch(`api/works`, {
        method: 'POST',
        headers: {'Accept': 'application/json'},
        body: JSON.stringify(this),
      });
      if (res.status == 200) {
        return await res.json();
      } else {
        throw new Error((await res.json()).errmsg);
      }
    }
  }

  async delete() {
    if (this.id) {
      const res = await fetch(`api/works/${this.id}`, {
        method: 'DELETE',
        headers: {'Accept': 'application/json'},
      });
      if (res.status == 200) {
        return true;
      } else {
        throw new Error((await res.json()).errmsg);
      }
    }
  }

  async stopTests() {
    if (this.id) {
      const res = await fetch(`api/works/${this.id}/tests`, {
        method: 'DELETE',
        headers: {'Accept': 'application/json'},
      });
      if (res.status == 200) {
        return true;
      } else {
        throw new Error((await res.json()).errmsg);
      }
    }
  }

  async addMissingGrades() {
    const res = await fetch(`api/works/${this.id}/grades`, {
      method: 'PATCH',
      headers: {'Accept': 'application/json'},
    });
    if (res.status == 200) {
      return (await res.json()).map((g) => new Grade(g));
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async getSubmission(uid) {
    const res = await fetch(uid?`api/users/${uid}/works/${this.id}/submission`:`api/works/${this.id}/submission`, {
      headers: {'Accept': 'application/json'}
    });
    if (res.status == 200) {
      return await res.json();
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async getMyTraces() {
    const res = await fetch(`api/works/${this.id}/traces`, {
      method: 'GET',
      headers: {'Accept': 'application/json'},
    });
    if (res.status == 200) {
      return await res.json();
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async getGrades() {
    const res = await fetch(`api/works/${this.id}/grades`, {
      method: 'GET',
      headers: {'Accept': 'application/json'},
    });
    if (res.status == 200) {
      return (await res.json()).map((g) => new Grade(g));
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }
}

export async function getWorks() {
  const res = await fetch(`api/works`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return (await res.json()).map((s) => new Work(s));
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getWork(wid) {
  const res = await fetch(`api/works/${wid}`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return new Work(await res.json());
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getUserRendu(baseurl, user) {
  const res = await fetch(baseurl.replace("%l", user.login).replace("%i", user.id))
  if (res.status == 200) {
    return await res.json();
  } else if (res.status == 404) {
    return await res.json();
  } else {
    throw new Error((await res.json()).errmsg);
  }
}
