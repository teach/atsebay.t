export async function getGradationRepositories() {
  let url = '/api/gradation_repositories';
  const res = await fetch(url, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return await res.json();
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function syncGradationRepositories() {
  let url = '/api/gradation_repositories/sync';
  const res = await fetch(url, {method: 'post', headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return await res.json();
  } else {
    throw new Error((await res.json()).errmsg);
  }
}
