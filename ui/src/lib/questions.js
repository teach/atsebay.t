import { Response } from './response';

export class QuestionProposal {
  constructor(res) {
    if (res) {
      this.update(res);
    }
  }

  update({ id, id_question, label }) {
    this.id = id;
    this.id_question = id_question;
    this.label = label;
    if (this.changed !== undefined)
      delete this.changed;
  }

  async save() {
    const res = await fetch(this.id?`api/questions/${this.id_question}/proposals/${this.id}`:`api/questions/${this.id_question}/proposals`, {
      method: this.id?'PUT':'POST',
      headers: {'Accept': 'application/json'},
      body: JSON.stringify(this),
    });
    if (res.status == 200) {
      const data = await res.json();
      this.update(data);
      return data;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async delete() {
    if (this.id) {
      const res = await fetch(`api/questions/${this.id_question}/proposals/${this.id}`, {
        method: 'DELETE',
        headers: {'Accept': 'application/json'},
      });
      if (res.status == 200) {
        return true;
      } else {
        throw new Error((await res.json()).errmsg);
      }
    }
  }
}

export class Question {
  constructor(res) {
    if (res) {
      this.update(res);
    }
  }

  update({ id, id_survey, title, description, desc_raw, placeholder, kind }) {
    this.id = id;
    this.id_survey = id_survey;
    this.title = title;
    this.description = description;
    this.desc_raw = desc_raw;
    this.placeholder = placeholder;
    this.kind = kind;
  }

  async getProposals(secret) {
    let url = `/questions/${this.id}/proposals`;
    if (secret) url = `/s/surveys/${this.id_survey}` + url + `?secret=${secret}`;
    const res = await fetch('api' + url, {
      method: 'GET',
      headers: {'Accept': 'application/json'},
    });
    if (res.status == 200) {
      const data = await res.json();
      if (data === null) {
        return [];
      } else {
        return (data).map((p) => new QuestionProposal(p))
      }
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async getMyResponse() {
    const res = await fetch(`api/questions/${this.id}/response`, {
      method: 'GET',
      headers: {'Accept': 'application/json'},
    });
    if (res.status == 200) {
      return new Response(await res.json());
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async getResponses(secret) {
    let url = `/surveys/${this.id_survey}/questions/${this.id}/responses`;
    if (secret) url = `/s` + url + `?secret=${secret}`;
    const res = await fetch('api' + url, {
      method: 'GET',
      headers: {'Accept': 'application/json'},
    });
    if (res.status == 200) {
      return (await res.json()).map((r) => new Response(r))
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async save() {
    const res = await fetch(this.id?`api/questions/${this.id}`:`api/surveys/${this.id_survey}/questions`, {
      method: this.id?'PUT':'POST',
      headers: {'Accept': 'application/json'},
      body: JSON.stringify(this),
    });
    if (res.status == 200) {
      const data = await res.json();
      this.update(data);
      return data;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async delete() {
    if (this.id) {
      // Start by deleting proposals
      const proposals = await this.getProposals();
      for (const p of proposals) {
        await p.delete();
      }

      const res = await fetch(`api/questions/${this.id}`, {
        method: 'DELETE',
        headers: {'Accept': 'application/json'},
      });
      if (res.status == 200) {
        return true;
      } else {
        throw new Error((await res.json()).errmsg);
      }
    }
  }
}

export async function getQuestion(qid) {
  const res = await fetch(`api/questions/${qid}`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return new Question(await res.json());
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getQuestions(sid) {
  const res = await fetch(`api/surveys/${sid}/questions`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    const data = await res.json();
    if (data === null) {
      return [];
    } else {
      return (data).map((q) => new Question(q))
    }
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getSharedQuestions(sid, secret) {
  const res = await fetch(`api/s/surveys/${sid}/questions?secret=${secret}`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    const data = await res.json();
    if (data === null) {
      return [];
    } else {
      return (data).map((q) => new Question(q))
    }
  } else {
    throw new Error((await res.json()).errmsg);
  }
}
