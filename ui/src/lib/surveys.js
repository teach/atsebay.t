import { getCorrectionTemplates } from './correctionTemplates';
import { getQuestions } from './questions';
import { Response } from './response';
import { Work } from './works';

export class Survey {
  constructor(res) {
    this.kind = "s";
    if (res) {
      this.update(res);
    }
  }

  update({ id, id_category, title, promo, group, shown, direct, corrected, start_availability, end_availability }) {
    this.id = id;
    this.id_category = id_category;
    this.title = title;
    this.promo = promo;
    this.group = group;
    this.shown = shown;
    this.direct = direct;
    this.corrected = corrected;
    if (this.start_availability != start_availability) {
      this.start_availability = start_availability;
      delete this.__start_availability;
    }
    if (this.end_availability != end_availability) {
      this.end_availability = end_availability;
      delete this.__end_availability;
    }
  }

  startAvailability() {
    if (!this.__start_availability) {
      this.__start_availability = new Date(this.start_availability)
    }
    return this.__start_availability
  }

  endAvailability() {
    if (!this.__end_availability) {
      this.__end_availability = new Date(this.end_availability)
    }
    return this.__end_availability
  }

  isFinished() {
    return this.endAvailability() < new Date();
  }

  async retrieveAnswers(id_user=null) {
    const res = await fetch(id_user?`api/users/${id_user}/surveys/${this.id}/responses`:`api/surveys/${this.id}/responses`, {
      method: 'GET',
      headers: {'Accept': 'application/json'},
    });
    if (res.status == 200) {
      return (await res.json()).map((r) => new Response(r));
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async submitAnswers(answer, id_user=null) {
    const res = await fetch(id_user?`api/users/${id_user}/surveys/${this.id}`:`api/surveys/${this.id}`, {
      method: 'POST',
      headers: {'Accept': 'application/json'},
      body: JSON.stringify(answer),
    });
    if (res.status == 200) {
      return await res.json();
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async share() {
    const res = await fetch(`api/surveys/${this.id}/shares`, {
      method: 'POST',
      headers: {'Accept': 'application/json'}
    });
    if (res.status == 200) {
      return await res.json();
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async save() {
    const res = await fetch(this.id?`api/surveys/${this.id}`:'api/surveys', {
      method: this.id?'PUT':'POST',
      headers: {'Accept': 'application/json'},
      body: JSON.stringify(this),
    });
    if (res.status == 200) {
      const data = await res.json()
      this.update(data);
      return data;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async duplicate() {
    if (this.id) {
      const oldSurveyId = this.id;
      delete this.id;
      const res = await fetch(`api/surveys`, {
        method: 'POST',
        headers: {'Accept': 'application/json'},
        body: JSON.stringify(this),
      });
      if (res.status == 200) {
        const response = await res.json();

        // Now recopy questions
        const questions = await getQuestions(oldSurveyId);
        for (const q of questions) {
          const oldQuestionId = q.id;

          // This will create a new question with the same parameters
          delete q.id;

          // Also alter id_survey
          q.id_survey = response.id;

          // This save will create
          const question = await q.save();

          // Revert to the old question ID to perform the next retrievals
          q.id = oldQuestionId;

          // Now recopy proposals
          if (q.kind == "mcq" || q.kind == "ucq") {
            const proposals = await q.getProposals();
            for (const p of proposals) {
              delete p.id;
              p.id_question = question.id;
              await p.save();
            }
          }

          // Now recopy correction templates
          const cts = await getCorrectionTemplates(oldQuestionId);
          for (const ct of cts) {
            delete ct.id;
            ct.id_question = question.id;
            ct.save();
          }
        }

        return response;
      } else {
        throw new Error((await res.json()).errmsg);
      }
    }
  }

  async delete() {
    if (this.id) {
      // Start by deleting questions
      const questions = await getQuestions(this.id);
      for (const q of questions) {
        await q.delete();
      }

      const res = await fetch(`api/surveys/${this.id}`, {
        method: 'DELETE',
        headers: {'Accept': 'application/json'},
      });
      if (res.status == 200) {
        return true;
      } else {
        throw new Error((await res.json()).errmsg);
      }
    }
  }
}

export async function getSurveys(allworks) {
  const res = await fetch(allworks?`api/all_works`:`api/surveys`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    if (allworks) {
      return (await res.json()).map((s) => {
        if (s.kind == "survey")
          return new Survey(s);
        else
          return new Work(s);
      });
    } else {
      return (await res.json()).map((s) => new Survey(s));
    }
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getSurvey(sid) {
  const res = await fetch(`api/surveys/${sid}`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return new Survey(await res.json());
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getSharedSurvey(sid, secret) {
  const res = await fetch(`api/s/surveys/${sid}?secret=${secret}`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return new Survey(await res.json());
  } else {
    throw new Error((await res.json()).errmsg);
  }
}
