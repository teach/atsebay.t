export async function getCategories() {
  let url = '/api/categories';
  const res = await fetch(url, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return (await res.json()).map((r) => new Category(r));
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export class Category {
  constructor(res) {
    if (res) {
      this.update(res);
    }
  }

  update({ id, label, promo, expand }) {
    this.id = id;
    this.label = label;
    this.promo = promo;
    this.expand = expand;
  }

  async save() {
    const res = await fetch(this.id?`api/categories/${this.id}`:'api/categories', {
      method: this.id?'PUT':'POST',
      headers: {'Accept': 'application/json'},
      body: JSON.stringify(this),
    });
    if (res.status == 200) {
      const data = await res.json()
      this.update(data);
      return data;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async delete() {
    const res = await fetch(`api/categories/${this.id}`, {
      method: 'DELETE',
      headers: {'Accept': 'application/json'},
    });
    if (res.status == 200) {
      return true;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }
}

export async function getCategory(cid) {
  const res = await fetch(`api/categories/${cid}`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return new Category(await res.json());
  } else {
    throw new Error((await res.json()).errmsg);
  }
}
