export class Grade {
  constructor(res) {
    if (res) {
      this.update(res);
    }
  }

  update({ id, login, id_user, id_work, date, score, comment }) {
    this.id = id;
    this.login = login;
    this.id_user = id_user;
    this.id_work = id_work;
    this.date = date;
    this.score = score;
    this.comment = comment;
  }

  async save() {
    const res = await fetch(`api/works/${this.id_work}/grades/${this.id}`, {
      method: 'PUT',
      headers: {'Accept': 'application/json'},
      body: JSON.stringify(this),
    });
    if (res.status == 200) {
      const data = await res.json()
      this.update(data);
      return data;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async delete() {
    if (this.id) {
      const res = await fetch(`api/works/${this.id_work}/grades/${this.id}`, {
        method: 'DELETE',
        headers: {'Accept': 'application/json'},
      });
      if (res.status == 200) {
        return true;
      } else {
        throw new Error((await res.json()).errmsg);
      }
    }
  }

  async redoGradation() {
    const res = await fetch(this.id_user?`api/users/${this.id_user}/works/${this.id_work}/grades/${this.id}/traces`:`api/works/${this.id_work}/grades/${this.id}/traces`, {
      method: 'POST',
      headers: {'Accept': 'application/json'},
    });
    if (res.status == 200) {
      return await res.json();
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async gradationStatus() {
    const res = await fetch(this.id_user?`api/users/${this.id_user}/works/${this.id_work}/grades/${this.id}/status`:`api/works/${this.id_work}/grades/${this.id}/status`, {
      method: 'GET',
      headers: {'Accept': 'application/json'},
    });
    if (res.status == 200) {
      return await res.json();
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }
}
