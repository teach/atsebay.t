export class Response {
  constructor(res) {
    if (res) {
      this.update(res);
    }
  }

  update({ id, id_question, id_user, value, time_submit, score, score_explaination, id_corrector, time_scored, time_reported }) {
    this.id = id;
    this.id_question = id_question;
    this.id_user = id_user;
    this.value = value;
    this.time_submit = time_submit;
    this.score = score;
    this.score_explaination = score_explaination;
    this.id_corrector = id_corrector;
    this.time_scored = time_scored;
    this.time_reported = time_reported;
  }

  async report(survey) {
    const res = await fetch(`api/surveys/${survey.id}/responses/${this.id}/report`, {
      method: 'POST',
      headers: {'Accept': 'application/json'},
    });
    if (res.status == 200) {
      const data = await res.json();
      this.update(data);
      return data;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async save() {
    const res = await fetch(`api/questions/${this.id_question}/responses/${this.id}`, {
      method: 'PUT',
      headers: {'Accept': 'application/json'},
      body: JSON.stringify(this),
    });
    if (res.status == 200) {
      const data = await res.json();
      this.update(data);
      return data;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }
}
