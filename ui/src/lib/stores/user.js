import { writable } from 'svelte/store';

function createUserStore() {
  const { subscribe, set, update } = writable(undefined);

  return {
    subscribe,
    set: (auth) => {
      update((m) => auth);
    },
    update: (res_auth) => {
      if (res_auth.status === 200) {
        res_auth.json().then((auth) => {
          update((m) => (Object.assign(m?m:{}, auth)));
        });
      } else if (res_auth.status >= 400 && res_auth.status < 500) {
        update((m) => (null));
      }
    },
  };
}

export async function refresh_auth() {
  const res = await fetch('api/auth', {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    const auth = await res.json();
    user.set(auth);
  } else {
    user.set(null);
  }
}

export const user = createUserStore();
