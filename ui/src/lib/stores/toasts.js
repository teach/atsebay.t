import { writable } from 'svelte/store';

function createToastsStore() {
  const { subscribe, set, update } = writable({toasts: []});

  const addToast = (o) => {
    o.timestamp = new Date();

    o.close = () => {
      update((i) => {
        i.toasts = i.toasts.filter((j) => {
          return !(j.title === o.title && j.msg === o.msg && j.timestamp === o.timestamp)
        });
        return i;
      });
    }

    update((i) => {
      i.toasts.unshift(o);
      return i;
    });

    o.cancel = setTimeout(o.close, o.dismiss?o.dismiss:5000);
  };

  const addErrorToast = (o) => {
    if (!o.title) o.title = 'Une erreur est survenue !';
    if (!o.color) o.color = 'danger';

    return addToast(o);
  };

  return {
    subscribe,
    addToast,
    addErrorToast,
  };

}

export const ToastsStore = createToastsStore();
