export class CorrectionTemplate {
  constructor(res) {
    if (res) {
      this.update(res);
    }
  }

  update({ id, id_question, label, regexp, score, score_explaination }) {
    this.id = id;
    this.id_question = id_question;
    this.regexp = regexp;
    this.label = label;
    this.score = score;
    this.score_explaination = score_explaination;
  }

  async getCorrections() {
    if (this.id) {
      const res = await fetch(`api/questions/${this.id_question}/corrections/${this.id}`, {
        method: 'GET',
        headers: {'Accept': 'application/json'},
      });
      if (res.status == 200) {
        return await res.json();
      } else {
        throw new Error((await res.json()).errmsg);
      }
    }
  }

  async save() {
    const res = await fetch(this.id?`api/questions/${this.id_question}/corrections/${this.id}`:`api/questions/${this.id_question}/corrections`, {
      method: this.id?'PUT':'POST',
      headers: {'Accept': 'application/json'},
      body: JSON.stringify(this),
    });
    if (res.status == 200) {
      const data = await res.json();
      this.update(data);
      return data;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async delete() {
    if (this.id) {
      const res = await fetch(`api/questions/${this.id_question}/corrections/${this.id}`, {
        method: 'DELETE',
        headers: {'Accept': 'application/json'},
      });
      if (res.status == 200) {
        return true;
      } else {
        throw new Error((await res.json()).errmsg);
      }
    } else {
      return true;
    }
  }
}

export async function getCorrectionTemplates(qid) {
  const res = await fetch(`api/questions/${qid}/corrections`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    const data = await res.json();
    if (data === null) {
      return [];
    } else {
      return (data).map((c) => new CorrectionTemplate(c))
    }
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function autoCorrection(id_user, my_tpls) {
  const res = await fetch(`api/users/${id_user}/corrections`, {
    method: 'PUT',
    headers: {'Accept': 'application/json'},
    body: JSON.stringify(my_tpls),
  });
  if (res.status == 200) {
    return await res.json();
  } else {
    throw new Error((await res.json()).errmsg);
  }
}
