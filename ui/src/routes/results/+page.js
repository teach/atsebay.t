export async function load({ url }) {
  return {
    secret: url.searchParams.get("secret"),
    idsurvey: url.searchParams.get("survey"),
    exportview_list: url.searchParams.get("graph_list")?false:true,
  };
}
