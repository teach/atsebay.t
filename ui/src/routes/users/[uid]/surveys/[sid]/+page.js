export async function load({ params }) {
  return {
    sid: params.sid,
    uid: params.uid,
  };
}
