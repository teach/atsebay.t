export async function load({ params }) {
  return {
    promo: parseInt(params.promo),
    cid: parseInt(params.cid),
  };
}
