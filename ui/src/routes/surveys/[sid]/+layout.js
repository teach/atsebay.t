import { getSurvey } from '$lib/surveys';

export async function load({ params }) {
  const survey = getSurvey(params.sid);

  return {
    survey,
  };
}
