export async function load({ parent }) {
  const stuff = await parent();

  return {
    survey: stuff.survey,
  };
}
