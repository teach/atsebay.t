export async function load({ params, parent }) {
  const stuff = await parent();

  return {
    survey: stuff.survey,
    rid: params.rid,
  };
}
