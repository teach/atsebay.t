export async function load({ parent, params }) {
  const stuff = await parent();

  return {
    survey: stuff.survey,
    sid: params.sid,
  };
}
