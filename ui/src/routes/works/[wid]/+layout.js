import { getWork } from '$lib/works';

export async function load({ params }) {
  const work = getWork(params.wid);

  return {
    work,
  };
}
