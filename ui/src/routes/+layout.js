import { refresh_auth, user } from '$lib/stores/user';

export const ssr = false;

let refresh_interval_auth = null;

export async function load({ url }) {
  refresh_interval_auth = setInterval(refresh_auth, Math.floor(Math.random() * 200000) + 200000);
  refresh_auth();

  const rroutes = url.pathname.split('/');

  return {
    rroute: rroutes.length>1?rroutes[1]:'',
  };
}
