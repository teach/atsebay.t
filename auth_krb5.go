package main

import (
	"log"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/jcmturner/gokrb5/v8/client"
	"github.com/jcmturner/gokrb5/v8/config"
	"github.com/jcmturner/gokrb5/v8/iana/etypeID"
	"github.com/jcmturner/gokrb5/v8/krberror"
)

func parseETypes(s []string, w bool) []int32 {
	var eti []int32
	for _, et := range s {
		if !w {
			var weak bool
			for _, wet := range strings.Fields(config.WeakETypeList) {
				if et == wet {
					weak = true
					break
				}
			}
			if weak {
				continue
			}
		}
		i := etypeID.EtypeSupported(et)
		if i != 0 {
			eti = append(eti, i)
		}
	}
	return eti
}

func checkAuthKrb5(c *gin.Context) {
	var lf loginForm
	if err := c.ShouldBindJSON(&lf); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": err.Error()})
		return
	}

	// Convert email to login
	lf.Login = strings.TrimSuffix(lf.Login, "@epita.fr")

	if !allowLocalAuth {
		found := false
		for _, u := range localAuthUsers {
			if lf.Login == u {
				found = true
				break
			}
		}

		if !userExists(lf.Login) && !found {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"errmsg": "You are not allowed to log you in this way. Please use OpenID Connect."})
			return
		}
	}

	cnf := config.New()
	cnf.LibDefaults.DNSLookupKDC = true
	cnf.LibDefaults.DNSLookupRealm = true
	cnf.LibDefaults.DefaultTGSEnctypeIDs = parseETypes(cnf.LibDefaults.DefaultTGSEnctypes, cnf.LibDefaults.AllowWeakCrypto)
	cnf.LibDefaults.DefaultTktEnctypeIDs = parseETypes(cnf.LibDefaults.DefaultTktEnctypes, cnf.LibDefaults.AllowWeakCrypto)
	cnf.LibDefaults.PermittedEnctypeIDs = parseETypes(cnf.LibDefaults.PermittedEnctypes, cnf.LibDefaults.AllowWeakCrypto)

	cl := client.NewWithPassword(lf.Login, "CRI.EPITA.FR", lf.Password, cnf)
	if err := cl.Login(); err != nil {
		if errk, ok := err.(krberror.Krberror); ok {
			if errk.RootCause == krberror.NetworkingError {
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": "Authentication system unavailable, please retry."})
				return
			} else if errk.RootCause == krberror.KDCError {
				c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"errmsg": "Invalid username or password"})
				return
			}
		}
		log.Println("Unable to login through Kerberos: unknown error:", err)
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"errmsg": "Invalid credentials (unknown error)."})
		return
	}

	if usr, err := completeAuth(c, lf.Login, lf.Login+"@epita.fr", "", "", 0, "", nil); err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"errmsg": err.Error()})
		return
	} else {
		c.JSON(http.StatusOK, authToken{User: usr, CurrentPromo: currentPromo})
	}
}
