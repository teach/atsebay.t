package main

import (
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func declareAPIAuthAsksRoutes(router *gin.RouterGroup) {
	router.POST("/ask", func(c *gin.Context) {
		u := c.MustGet("LoggedUser").(*User)
		s := c.MustGet("survey").(*Survey)

		var ask Ask
		if err := c.ShouldBindJSON(&ask); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": err.Error()})
			return
		}

		a, err := s.NewAsk(u.Id, ask.Content)
		if err != nil {
			log.Println("Unable to NewAsk:", err)
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": "Unable to register your question. Please try again in a few moment."})
			return
		}

		if s.Direct != nil {
			s.WSAdminWriteAll(WSMessage{Action: "new_ask", UserId: &u.Id, QuestionId: &a.Id, Response: ask.Content})
		}

		c.JSON(http.StatusOK, a)
	})
}

func declareAPIAdminAsksRoutes(router *gin.RouterGroup) {
	router.GET("/ask", func(c *gin.Context) {
		s := c.MustGet("survey").(*Survey)

		asks, err := s.GetAsks(true)
		if err != nil {
			log.Println("Unable to GetAsks:", err)
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": "Unable to retrieve asks. Please try again later."})
			return
		}

		c.JSON(http.StatusOK, asks)
	})
}

type Ask struct {
	Id       int64     `json:"id"`
	IdSurvey int64     `json:"id_survey"`
	IdUser   int64     `json:"id_user"`
	Date     time.Time `json:"date"`
	Content  string    `json:"content"`
	Answered bool      `json:"answered,omitempty"`
}

func (s *Survey) GetAsks(unansweredonly bool) (asks []Ask, err error) {
	cmp := ""
	if unansweredonly {
		cmp = " AND answered = 0"
	}

	if rows, errr := DBQuery("SELECT id_ask, id_survey, id_user, date, content, answered FROM survey_asks WHERE id_survey=?"+cmp, s.Id); errr != nil {
		return nil, errr
	} else {
		defer rows.Close()

		for rows.Next() {
			var a Ask
			if err = rows.Scan(&a.Id, &a.IdSurvey, &a.IdUser, &a.Date, &a.Content, &a.Answered); err != nil {
				return
			}
			asks = append(asks, a)
		}
		if err = rows.Err(); err != nil {
			return
		}

		return
	}
}

func GetAsk(id int) (a Ask, err error) {
	err = DBQueryRow("SELECT id_ask, id_survey, id_user, date, content, answered FROM survey_asks WHERE id_ask = ?", id).Scan(&a.Id, &a.IdSurvey, &a.IdUser, &a.Date, &a.Content, &a.Answered)
	return
}

func (s *Survey) NewAsk(id_user int64, content string) (Ask, error) {
	if res, err := DBExec("INSERT INTO survey_asks (id_survey, id_user, date, content) VALUES (?, ?, ?, ?)", s.Id, id_user, time.Now(), content); err != nil {
		return Ask{}, err
	} else if aid, err := res.LastInsertId(); err != nil {
		return Ask{}, err
	} else {
		return Ask{aid, s.Id, id_user, time.Now(), content, false}, nil
	}
}

func (a *Ask) Update() error {
	_, err := DBExec("UPDATE survey_asks SET id_survey = ?, id_user = ?, date = ?, content = ?, answered = ? WHERE id_ask = ?", a.IdSurvey, a.IdUser, a.Date, a.Content, a.Answered, a.Id)
	return err
}

func (a *Ask) Delete() (int64, error) {
	if res, err := DBExec("DELETE FROM survey_asks WHERE id_ask = ?", a.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func ClearAsks() (int64, error) {
	if res, err := DBExec("DELETE FROM survey_asks"); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}
