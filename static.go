package main

import (
	"io"
	"net/http"
	"net/url"
	"path"

	"github.com/gin-gonic/gin"
)

var DevProxy string

func serveOrReverse(forced_url string) func(c *gin.Context) {
	return func(c *gin.Context) {
		if DevProxy != "" {
			if u, err := url.Parse(DevProxy); err != nil {
				http.Error(c.Writer, err.Error(), http.StatusInternalServerError)
			} else {
				if forced_url != "" {
					u.Path = path.Join(u.Path, forced_url)
				} else {
					u.Path = path.Join(u.Path, c.Request.URL.Path)
				}

				if r, err := http.NewRequest(c.Request.Method, u.String(), c.Request.Body); err != nil {
					http.Error(c.Writer, err.Error(), http.StatusInternalServerError)
				} else if resp, err := http.DefaultClient.Do(r); err != nil {
					http.Error(c.Writer, err.Error(), http.StatusBadGateway)
				} else {
					defer resp.Body.Close()

					for key := range resp.Header {
						c.Writer.Header().Add(key, resp.Header.Get(key))
					}
					c.Writer.WriteHeader(resp.StatusCode)

					io.Copy(c.Writer, resp.Body)
				}
			}
		} else {
			if forced_url != "" {
				c.Request.URL.Path = forced_url
			}
			http.FileServer(Assets).ServeHTTP(c.Writer, c.Request)
		}
	}
}

func declareStaticRoutes(router *gin.Engine) {
	router.GET("/", serveOrReverse(""))
	router.GET("/_app/*_", serveOrReverse(""))
	router.GET("/auth/", serveOrReverse("/"))
	router.GET("/bug-bounty", serveOrReverse("/"))
	router.GET("/categories", serveOrReverse("/"))
	router.GET("/categories/*_", serveOrReverse("/"))
	router.GET("/donnees-personnelles", serveOrReverse("/"))
	router.GET("/grades", serveOrReverse("/"))
	router.GET("/grades/*_", serveOrReverse("/"))
	router.GET("/help", serveOrReverse("/"))
	router.GET("/keys", serveOrReverse("/"))
	router.GET("/results", serveOrReverse("/"))
	router.GET("/surveys", serveOrReverse("/"))
	router.GET("/surveys/*_", serveOrReverse("/"))
	router.GET("/users", serveOrReverse("/"))
	router.GET("/users/*_", serveOrReverse("/"))
	router.GET("/works", serveOrReverse("/"))
	router.GET("/works/*_", serveOrReverse("/"))
	router.GET("/css/*_", serveOrReverse(""))
	router.GET("/fonts/*_", serveOrReverse(""))
	router.GET("/img/*_", serveOrReverse(""))

	if DevProxy != "" {
		router.GET("/.svelte-kit/*_", serveOrReverse(""))
		router.GET("/node_modules/*_", serveOrReverse(""))
		router.GET("/@vite/*_", serveOrReverse(""))
		router.GET("/@fs/*_", serveOrReverse(""))
		router.GET("/src/*_", serveOrReverse(""))
	}
}
