package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

func declareAPIAdminHelpRoutes(router *gin.RouterGroup) {
	router.GET("/help", func(c *gin.Context) {
		nhs, err := getNeedHelps("WHERE date_treated IS NULL")
		if err != nil {
			log.Println("Unable to getNeedHelps:", err)
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": "An error occurs during need helps retrieval. Please retry."})
			return
		}

		c.JSON(http.StatusOK, nhs)
	})

	needhelpsRoutes := router.Group("/help/:hid")
	needhelpsRoutes.Use(needHelpHandler)

	needhelpsRoutes.PUT("", func(c *gin.Context) {
		current := c.MustGet("needhelp").(*NeedHelp)

		var new NeedHelp
		if err := c.ShouldBindJSON(&new); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": err.Error()})
			return
		}

		new.Id = current.Id

		if err := new.Update(); err != nil {
			log.Println("Unable to Update needhelp:", err)
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": fmt.Sprintf("An error occurs during needhelp entry updation: %s", err.Error())})
			return
		} else {
			c.JSON(http.StatusOK, new)
		}
	})
}

func declareAPIAuthHelpRoutes(router *gin.RouterGroup) {
	router.POST("/help", func(c *gin.Context) {
		u := c.MustGet("LoggedUser").(*User)

		nh, err := u.NewNeedHelp()
		if err != nil {
			log.Printf("Unable to NewNeedHelp(uid=%d): %s", u.Id, err)
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": "Sorry, something went wrong. Please retry in a few moment."})
			return
		}

		c.JSON(http.StatusOK, nh)
	})
}

func needHelpHandler(c *gin.Context) {
	if hid, err := strconv.Atoi(string(c.Param("hid"))); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": "Bad need help identifier."})
		return
	} else if nh, err := getNeedHelp(hid); err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"errmsg": "Need help entry not found."})
		return
	} else {
		c.Set("needhelp", nh)
		c.Next()
	}
}

type NeedHelp struct {
	Id          int64      `json:"id"`
	IdUser      int64      `json:"id_user"`
	Date        time.Time  `json:"date"`
	Comment     string     `json:"comment,omitempty"`
	DateTreated *time.Time `json:"treated,omitempty"`
}

func getNeedHelps(cond string) (nh []NeedHelp, err error) {
	if rows, errr := DBQuery("SELECT id_need_help, id_user, date, comment, date_treated FROM user_need_help " + cond); errr != nil {
		return nil, errr
	} else {
		defer rows.Close()

		for rows.Next() {
			var n NeedHelp
			if err = rows.Scan(&n.Id, &n.IdUser, &n.Date, &n.Comment, &n.DateTreated); err != nil {
				return
			}
			nh = append(nh, n)
		}
		if err = rows.Err(); err != nil {
			return
		}

		return
	}
}

func getNeedHelp(id int) (n *NeedHelp, err error) {
	n = new(NeedHelp)
	err = DBQueryRow("SELECT id_need_help, id_user, date, comment, date_treated FROM user_need_help WHERE id_need_help=?", id).Scan(&n.Id, &n.IdUser, &n.Date, &n.Comment, &n.DateTreated)
	return
}

func (u *User) NewNeedHelp() (NeedHelp, error) {
	if res, err := DBExec("INSERT INTO user_need_help (id_user, comment) VALUES (?, ?)", u.Id, ""); err != nil {
		return NeedHelp{}, err
	} else if hid, err := res.LastInsertId(); err != nil {
		return NeedHelp{}, err
	} else {
		return NeedHelp{hid, u.Id, time.Now(), "Ton appel a bien été entendu.", nil}, nil
	}
}

func (h *NeedHelp) Update() error {
	_, err := DBExec("UPDATE user_need_help SET id_user = ?, date = ?, comment = ?, date_treated = ? WHERE id_need_help = ?", h.IdUser, h.Date, h.Comment, h.DateTreated, h.Id)
	return err
}

func (h *NeedHelp) Delete() (int64, error) {
	if res, err := DBExec("DELETE FROM user_need_help WHERE id_need_help = ?", h.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func ClearNeedHelp() (int64, error) {
	if res, err := DBExec("DELETE FROM user_need_help"); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}
