package main

import (
	"context"
	"encoding/hex"
	"flag"
	"fmt"
	"log"
	"net/http"

	"golang.org/x/oauth2"

	"github.com/coreos/go-oidc/v3/oidc"
	"github.com/gin-gonic/gin"
)

var (
	oidcClientID    = ""
	oidcSecret      = ""
	oidcRedirectURL = "https://srs.nemunai.re"
	oauth2Config    oauth2.Config
	oidcVerifier    *oidc.IDTokenVerifier
	nextSessionMap  = map[string]string{}
)

func init() {
	flag.StringVar(&oidcClientID, "oidc-clientid", oidcClientID, "ClientID for OIDC")
	flag.StringVar(&oidcSecret, "oidc-secret", oidcSecret, "Secret for OIDC")
	flag.StringVar(&oidcRedirectURL, "oidc-redirect", oidcRedirectURL, "Base URL for the redirect after connection")
}

func initializeOIDC(router *gin.Engine) {
	router.GET("/auth/CRI", redirectOIDC_CRI)
	router.GET("/auth/complete", OIDC_CRI_complete)

	if oidcClientID != "" && oidcSecret != "" {
		provider, err := oidc.NewProvider(context.Background(), "https://cri.epita.fr")
		if err != nil {
			log.Fatal("Unable to setup oidc:", err)
		}

		oauth2Config = oauth2.Config{
			ClientID:     oidcClientID,
			ClientSecret: oidcSecret,
			RedirectURL:  oidcRedirectURL + baseURL + "/auth/complete",

			// Discovery returns the OAuth2 endpoints.
			Endpoint: provider.Endpoint(),

			// "openid" is a required scope for OpenID Connect flows.
			Scopes: []string{oidc.ScopeOpenID, "profile", "email", "epita"},
		}

		oidcConfig := oidc.Config{
			ClientID: oidcClientID,
		}
		oidcVerifier = provider.Verifier(&oidcConfig)
	}

}

func redirectOIDC_CRI(c *gin.Context) {
	session, err := NewSession()

	// Save next parameter
	if len(c.Request.URL.Query().Get("next")) > 0 {
		nextSessionMap[fmt.Sprintf("%x", session.Id)] = c.Request.URL.Query().Get("next")
	}

	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
		return
	}

	c.Redirect(http.StatusFound, oauth2Config.AuthCodeURL(hex.EncodeToString(session.Id)))
}

func OIDC_CRI_complete(c *gin.Context) {
	idsession, err := hex.DecodeString(c.Request.URL.Query().Get("state"))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": err.Error()})
		return
	}

	session, err := getSession(idsession)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": err.Error()})
		return
	}

	oauth2Token, err := oauth2Config.Exchange(c.Request.Context(), c.Request.URL.Query().Get("code"))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": "Failed to exchange token: " + err.Error()})
		return
	}
	rawIDToken, ok := oauth2Token.Extra("id_token").(string)
	if !ok {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": "No id_token field in oauth2 token."})
		return
	}
	idToken, err := oidcVerifier.Verify(c.Request.Context(), rawIDToken)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": "Failed to verify ID Token: " + err.Error()})
		return
	}

	var claims struct {
		Firstname       string                   `json:"given_name"`
		Lastname        string                   `json:"family_name"`
		Username        string                   `json:"preferred_username"`
		Email           string                   `json:"email"`
		Groups          []map[string]interface{} `json:"groups"`
		Campuses        []string                 `json:"campuses"`
		GraduationYears []uint                   `json:"graduation_years"`
	}
	if err := idToken.Claims(&claims); err != nil {
		log.Println("Unable to extract claims to Claims:", err.Error())
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": "Something goes wrong when analyzing your claims. Contact administrator to fix the issue."})
		return
	}

	groups := ","
	for _, g := range claims.Groups {
		if slug, ok := g["slug"]; ok {
			groups += slug.(string) + ","
		}
	}

	var promo uint
	if len(claims.GraduationYears) > 0 {
		for _, gy := range claims.GraduationYears {
			if gy > promo {
				promo = gy
			}
		}
	}

	if _, err := completeAuth(c, claims.Username, claims.Email, claims.Firstname, claims.Lastname, promo, groups, session); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
		return
	}

	// Retrieve next URL associated with session
	if next, ok := nextSessionMap[fmt.Sprintf("%x", session.Id)]; ok {
		c.Redirect(http.StatusFound, next)
		delete(nextSessionMap, fmt.Sprintf("%x", session.Id))
	} else {
		c.Redirect(http.StatusFound, "/")
	}

}
