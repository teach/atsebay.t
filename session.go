package main

import (
	"crypto/rand"
	"encoding/json"
	"time"
)

type Session struct {
	Id      []byte    `json:"id"`
	IdUser  *int64    `json:"login"`
	Time    time.Time `json:"time"`
	changed bool
	Values  map[string]interface{} `json:"values"`
}

func getSession(id []byte) (s *Session, err error) {
	s = new(Session)
	var val string
	err = DBQueryRow("SELECT id_session, id_user, time, val FROM user_sessions WHERE id_session=?", id).Scan(&s.Id, &s.IdUser, &s.Time, &val)

	err = json.Unmarshal([]byte(val), &s.Values)
	return
}

func NewSession() (*Session, error) {
	session_id := make([]byte, 255)
	if _, err := rand.Read(session_id); err != nil {
		return nil, err
	} else if _, err := DBExec("INSERT INTO user_sessions (id_session, time, val) VALUES (?, ?, '{}')", session_id, time.Now()); err != nil {
		return nil, err
	} else {
		return &Session{session_id, nil, time.Now(), false, map[string]interface{}{}}, nil
	}
}

func (user *User) NewSession() (*Session, error) {
	session_id := make([]byte, 255)
	if _, err := rand.Read(session_id); err != nil {
		return nil, err
	} else if _, err := DBExec("INSERT INTO user_sessions (id_session, id_user, time, val) VALUES (?, ?, ?, '{}')", session_id, user.Id, time.Now()); err != nil {
		return nil, err
	} else {
		return &Session{session_id, &user.Id, time.Now(), false, map[string]interface{}{}}, nil
	}
}

func (s *Session) SetUser(user *User) (*Session, error) {
	s.IdUser = &user.Id
	_, err := s.Update()
	s.changed = false
	return s, err
}

func (s *Session) GetKey(key string) (v interface{}, ok bool) {
	v, ok = s.Values[key]
	return
}

func (s *Session) DeleteKey(key string) {
	delete(s.Values, key)
	s.changed = true
}

func (s *Session) SetKey(key string, val interface{}) {
	s.Values[key] = val
	s.changed = true
}

func (s *Session) HasChanged() bool {
	return s.changed
}

func (s *Session) Update() (int64, error) {
	if val, err := json.Marshal(s.Values); err != nil {
		return 0, err
	} else if res, err := DBExec("UPDATE user_sessions SET id_user = ?, time = ?, val = ? WHERE id_session = ?", s.IdUser, s.Time, string(val), s.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func (s Session) Delete() (int64, error) {
	if res, err := DBExec("DELETE FROM user_sessions WHERE id_session = ?", s.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func ClearSession() (int64, error) {
	if res, err := DBExec("DELETE FROM user_sessions"); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}
