angular.module("AtsebaytApp", ["ngRoute", "ngResource", "ngSanitize"])
    .config(function($routeProvider, $locationProvider) {
	$routeProvider
	    .when("/auth", {
		controller: "AuthController",
		templateUrl: "views/auth.html"
	    })
	    .when("/grades", {
		controller: "GradesController",
		templateUrl: "views/grades.html"
	    })
	    .when("/surveys", {
		controller: "SurveysController",
		templateUrl: "views/surveys.html"
	    })
	    .when("/surveys/:surveyId", {
		controller: "SurveyController",
		templateUrl: "views/survey.html"
	    })
	    .when("/surveys/:surveyId/responses", {
		controller: "SurveyController",
		templateUrl: "views/responses.html"
	    })
	    .when("/surveys/:surveyId/responses/:questId", {
		controller: "QuestionController",
		templateUrl: "views/correction.html"
	    })
	    .when("/users", {
		controller: "UsersController",
		templateUrl: "views/users.html"
	    })
	    .when("/users/:userId", {
		controller: "UserController",
		templateUrl: "views/user.html"
	    })
	    .when("/users/:userId/surveys/:surveyId", {
		controller: "SurveyController",
		templateUrl: "views/survey.html"
	    })
	    .when("/", {
		controller: "SurveysController",
		templateUrl: "views/home.html"
	    });
	$locationProvider.html5Mode(true);
    });

angular.module("AtsebaytApp")
    .factory("AllResponses", function($resource) {
        return $resource("/api/surveys/:surveyId/questions/:questId/responses/:respId", { surveyId: '@id', questId: '@id', respId: '@id' }, {
	    'update': {method: 'PUT'},
	})
    })
    .factory("CorrectionTemplate", function($resource) {
        return $resource("/api/surveys/:surveyId/questions/:questId/corrections/:correctId", { surveyId: '@id', questId: '@id', correctId: '@id' }, {
	    'update': {method: 'PUT'},
	})
    })
    .factory("MyResponse", function($resource) {
        return $resource("/api/surveys/:surveyId/responses/:respId", { surveyId: '@id', respId: '@id' })
    })
    .factory("UserResponses", function($resource) {
        return $resource("/api/users/:userId/surveys/:surveyId/responses/:respId", { userId: '@id', surveyId: '@id', respId: '@id' })
    })
    .factory("Grades", function($resource) {
        return $resource("/api/grades")
    })
    .factory("Survey", function($resource) {
	return $resource("/api/surveys/:surveyId", { surveyId: '@id' }, {
	    'update': {method: 'PUT'},
	})
    })
    .factory("SurveyGrades", function($resource) {
	return $resource("/api/surveys/:surveyId/grades", { surveyId: '@id' })
    })
    .factory("SurveyScore", function($resource) {
	return $resource("/api/surveys/:surveyId/score", { surveyId: '@id' })
    })
    .factory("SurveyUserScore", function($resource) {
        return $resource("/api/users/:userId/surveys/:surveyId/score", { userId: '@id', surveyId: '@id' })
    })
    .factory("SurveyUserGrades", function($resource) {
        return $resource("/api/users/:userId/surveys/:surveyId/grades", { userId: '@id', surveyId: '@id' })
    })
    .factory("SurveyQuest", function($resource) {
        return $resource("/api/surveys/:surveyId/questions/:questId", { surveyId: '@id', questId: '@id' }, {
	    'update': {method: 'PUT'},
	})
    })
    .factory("QuestProposal", function($resource) {
        return $resource("/api/surveys/:surveyId/questions/:questId/proposals/:proposalId", { surveyId: '@id', questId: '@id', proposalId: '@id' }, {
	    'update': {method: 'PUT'},
	})
    })
    .factory("User", function($resource) {
	return $resource("/api/users/:userId", { userId: '@id' }, {
	    'update': {method: 'PUT'},
	})
    });

angular.module("AtsebaytApp")
  .directive('integer', function() {
    return {
      require: 'ngModel',
      link: function(scope, ele, attr, ctrl){
	ctrl.$parsers.unshift(function(viewValue){
	  return parseInt(viewValue, 10);
	});
      }
    };
  })

  .component('toast', {
    bindings: {
      date: '=',
      msg: '=',
      title: '=',
      variant: '=',
    },
    controller: function($element) {
      $element.children(0).toast('show')
    },
    template: `<div class="toast mb-2" role="alert" aria-live="assertive" aria-atomic="true" data-delay="7000">
	<div class="toast-header">
          <span ng-if="$ctrl.variant" class="badge badge-pill badge-{{ $ctrl.variant }}" style="padding: .25em .66em">&nbsp;</span>&nbsp;
	  <strong class="mr-auto" ng-bind="$ctrl.title"></strong>
	  <small class="text-muted" ng-bind="$ctrl.date">just now</small>
	  <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
	    <span aria-hidden="true">&times;</span>
	  </button>
	</div>
	<div class="toast-body" ng-bind-html="$ctrl.msg"></div>
      </div>`
  })

  .component('surveyList', {
    bindings: {
      surveys: '=',
      islogged: '=',
      isadmin: '=',
    },
    controller: function($location, $rootScope) {
      this.now = Date.now();
      this.show = function(id) {
	if ($rootScope.isLogged) {
	  $location.url("surveys/" + id);
	} else {
	  $rootScope.addToast({
	    variant: "danger",
	    title: "Authentification requise",
	    msg: "Vous devez être connecté pour accéder aux questionnaires.",
	  });
	  $location.url("auth");
	}
      };
    },
    template: `<table class="table table-striped table-hover">
  <thead>
    <tr>
      <th>Intitulé</th>
      <th>État</th>
      <th>Date</th>
      <th ng-if="$ctrl.islogged">Score</th>
    </tr>
  </thead>
  <tbody style="cursor: pointer;">
    <tr ng-repeat="survey in $ctrl.surveys" ng-if="survey.shown" ng-click="$ctrl.show(survey.id)">
      <td>{{ survey.title }}</td>
      <td class="bg-info" ng-if="survey.start_availability > $ctrl.now">Prévu</td>
      <td class="bg-warning" ng-if="survey.start_availability <= $ctrl.now && survey.end_availability >= $ctrl.now">En cours</td>
      <td class="bg-primary" ng-if="survey.end_availability < $ctrl.now && !survey.corrected">Terminé</td>
      <td class="bg-success" ng-if="survey.end_availability < $ctrl.now && survey.corrected">Corrigé</td>
      <td ng-if="survey.start_availability > $ctrl.now">{{ survey.start_availability | date: "medium" }} <svg class="bi bi-arrow-bar-right" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M12.146 6.646a.5.5 0 01.708 0l3 3a.5.5 0 010 .708l-3 3a.5.5 0 01-.708-.708L14.793 10l-2.647-2.646a.5.5 0 010-.708z" clip-rule="evenodd"></path><path fill-rule="evenodd" d="M8 10a.5.5 0 01.5-.5H15a.5.5 0 010 1H8.5A.5.5 0 018 10zm-2.5 6a.5.5 0 01-.5-.5v-11a.5.5 0 011 0v11a.5.5 0 01-.5.5z" clip-rule="evenodd"></path></svg></td>
      <td ng-if="survey.start_availability <= $ctrl.now"><svg class="bi bi-arrow-bar-left" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M7.854 6.646a.5.5 0 00-.708 0l-3 3a.5.5 0 000 .708l3 3a.5.5 0 00.708-.708L5.207 10l2.647-2.646a.5.5 0 000-.708z" clip-rule="evenodd"></path><path fill-rule="evenodd" d="M12 10a.5.5 0 00-.5-.5H5a.5.5 0 000 1h6.5a.5.5 0 00.5-.5zm2.5 6a.5.5 0 01-.5-.5v-11a.5.5 0 011 0v11a.5.5 0 01-.5.5z" clip-rule="evenodd"></path></svg> {{ survey.end_availability | date: "medium" }}</td>
      <td ng-if="$ctrl.islogged && !survey.corrected">N/A</td>
      <td ng-if="$ctrl.islogged && survey.corrected" ng-controller="ScoreController">{{ score.score }}</td>
    </tr>
  </tbody>
  <tfoot ng-if="$ctrl.isadmin">
    <tr>
      <td colspan="4">
	<a href="surveys/new" class="btn btn-sm btn-primary">Ajouter un questionnaire</a>
      </td>
    </tr>
  </tfoot>
</table>`
  })


  .component('surveyBadges', {
    bindings: {
      survey: '=',
    },
    controller: function() {
      var now = Date.now()
      this.test = function(a, b) {
	return Date.parse(a) > now;
      }
    },
    template: `<span class="badge badge-danger" ng-if="$ctrl.test($ctrl.survey.start_availability)">Prévu</span>
<span class="badge badge-warning" ng-if="!$ctrl.test($ctrl.survey.start_availability) && $ctrl.test($ctrl.survey.end_availability)">En cours</span>
<span class="badge badge-primary" ng-if="!$ctrl.test($ctrl.survey.end_availability)">Clos</span>
<span class="badge badge-success" ng-if="$ctrl.survey.corrected">Corrigé</span>`
  });

angular.module("AtsebaytApp")
    .run(function($rootScope, $interval, $http) {
	$rootScope.checkLoginState = function() {
	    $http({
		method: 'GET',
		url: "api/auth",
	    }).then(function(response) {
		$rootScope.isLogged = response.data;
		$rootScope.user = response.data;
	    }, function(response) {
		$rootScope.isLogged = false;
	    });
	};
	$rootScope.checkLoginState();
	$interval($rootScope.checkLoginState, 20000);

        $rootScope.switchAdminMode = function() {
	  var tmp = $rootScope.user.is_admin
	  $rootScope.user.is_admin = $rootScope.user.was_admin || false;
	  $rootScope.user.was_admin = tmp;
	}

        $rootScope.toasts = [];

        $rootScope.addToast = function(toast) {
	  $rootScope.toasts.unshift(toast);
        }

	$rootScope.disconnectCurrentUser = function() {
	    $http({
		method: 'POST',
		url: "api/auth/logout"
	    }).then(function(response) {
		$rootScope.isLogged = false;
		$rootScope.user = null;
	    });
	}
    })

    .controller("AuthController", function($scope, $rootScope, $http, $location) {
	$scope.auth = {
	    "username": "",
	    "password": "",
	};

	$scope.logmein = function() {
	    $scope.pleaseWait = true;
	    $http({
		method: 'POST',
		url: "api/auth",
		data: $scope.auth
	    }).then(function(response) {
		$scope.pleaseWait = false;
		$rootScope.checkLoginState();
		$location.url("/");
	    }, function(response) {
		$scope.pleaseWait = false;
		if (response.data)
	          $scope.addToast({
	            variant: "danger",
	            title: "Connexion impossible",
	            msg: (response.data ? response.data.errmsg : "Impossible de contacter le serveur"),
	          });
	    });
	}
    })

    .controller("SurveysController", function($scope, $rootScope, Survey, $location) {
      $rootScope.qactive = $location.$$path == "/surveys";
      $rootScope.uactive = $location.$$path.indexOf("/users") != -1;
      $scope.surveys = Survey.query();
      $scope.surveys.$promise.then(function(data) {
	data.forEach(function(d,k) {
	  data[k].start_availability = Date.parse(data[k].start_availability)
	  data[k].end_availability = Date.parse(data[k].end_availability)
	})
      })
      $scope.showUserSurvey = function() {
	$location.url("users/" + this.user.id + "/surveys/" + this.survey.id);
      }
    })

  .controller("SurveyController", function($scope, $rootScope, Survey, SurveyQuest, $routeParams, $location) {
      $rootScope.qactive = true;
      $rootScope.uactive = false;
      $scope.survey = Survey.get({ surveyId: $routeParams.surveyId });
      $scope.survey.$promise.then(function(survey) {
        survey.readonly = Date.now() > Date.parse(survey.end_availability)
      })

      $scope.saveSurvey = function() {
	if (this.survey.id) {
	  this.survey.$update(function(v) {
	    $scope.survey = Survey.get({ surveyId: $routeParams.surveyId });
	  });
	} else {
	  this.survey.$save(function() {
	    $location.url("surveys/" + $scope.survey.id);
	  });
	}
      }

      $scope.duplicateSurvey = function() {
        var bakSurveyId = this.survey.id;
	delete this.survey.id;
	this.survey.$save(function() {
          // Now recopy questions
          var questions = SurveyQuest.query({ surveyId: bakSurveyId });
          questions.$promise.then(function (questions) {
            questions.forEach(function (question) {
              delete question.id
	      question.$save({ surveyId: $scope.survey.id });
            })
            $location.url("surveys/" + $scope.survey.id);
          })
	});
      }

      $scope.editSurvey = function() {
	this.survey.edit = true;
      }

      $scope.deleteSurvey = function() {
	this.survey.$remove(function() { $location.url("/surveys/");});
      }

      $scope.showResponses = function() {
	$location.url("surveys/" + this.survey.id + "/responses/" + this.question.id );
      }
    })

    .controller("SurveyGradesController", function($scope, SurveyGrades) {
      $scope.grades = SurveyGrades.get({ surveyId: $scope.survey.id })
      $scope.mean = 0;
      $scope.grades.$promise.then(function (grades) {
        var sum = 0
        var total = 0
        for (var gid in grades) {
          if (parseInt(gid, 10) > 0) {
            total++
            if (grades[gid]) {
              sum += grades[gid]
            }
          }
        }
        if (total > 0) {
          $scope.mean = sum/total
        }
      })
    })

    .controller("ResponsesController", function($scope, AllResponses, $rootScope, $location) {
      $scope.responses = AllResponses.query({ surveyId: $scope.survey.id, questId: $scope.question.id });
      $scope.responses.$promise.then(function (responses) {
        if (!$rootScope.usersResponses) {
          $rootScope.usersResponses = {}
        }
        responses.forEach(function (response) {
          if (!response.time_scored) {
            if ($rootScope.usersResponses[response.id_user] === undefined) {
              $rootScope.usersResponses[response.id_user] = []
            }
            $rootScope.usersResponses[response.id_user].push(response.id_question)
          }
        });
      })

      $rootScope.showUserSurvey = function () {
        $location.url("users/" + this.id_user + "/surveys/" + this.survey.id);
      }
    })

    .controller("ScoreController", function($scope, SurveyScore) {
      $scope.score = SurveyScore.get({ surveyId: $scope.survey.id })
    })

    .controller("UserScoreController", function($scope, SurveyUserScore) {
      $scope.score = SurveyUserScore.get({ userId: $scope.user.id, surveyId: $scope.survey.id })
    })

    .controller("UserGradesController", function($scope, SurveyUserGrades) {
      $scope.grades = SurveyUserGrades.get({ userId: $scope.user.id, surveyId: $scope.survey.id })
      $scope.avancement = 0;
      $scope.grades.$promise.then(function (grades) {
        var answered = 0
        var total = 0
        for (var gid in grades) {
          if (parseInt(gid, 10) > 0) {
            total++
            if (grades[gid]) {
              answered++
            }
          }
        }
        if (total > 0) {
          $scope.avancement = answered/total
        }
      })
    })

    .controller("GradesController", function($scope, $rootScope, Grades, Survey, User, $location) {
      $scope.users = User.query();
      $scope.surveys = Survey.query();
      $scope.grades = Grades.get();

      $scope.showUser = function() {
        $location.url("users/" + this.user.id);
      }
    })

    .controller("UsersController", function($scope, $rootScope, User, $location) {
      $rootScope.qactive = false;
      $rootScope.uactive = true;
      $scope.users = User.query()

      $scope.showUser = function() {
        $location.url("users/" + this.user.id);
      }
    })

    .controller("UserController", function($scope, $routeParams, $rootScope, User) {
      $rootScope.qactive = false;
      $rootScope.uactive = true;
      if ($scope.id_user) {
        $scope.user = User.get({ userId: $scope.id_user})
      } else {
        $scope.user = User.get({ userId: $routeParams.userId})
      }
    })

    .controller("QuestionController", function($scope, Survey, SurveyQuest, AllResponses, UserResponses, CorrectionTemplate, $http, $routeParams) {
      $scope.notCorrected = true
      $scope.highlight = ''

      $scope.chHilight = function () {
        var words = $scope.highlight.split(',').join('|')

        $('.card-text').each(function (k, i) {
          var e = $(i)
          e.html(e.text().replace(new RegExp('(' + words + ')', 'gi'), '<span class="bg-warning">$1</span>'))
        })
      }

      $scope.survey = Survey.get({ surveyId: $routeParams.surveyId });
      $scope.survey.$promise.then(function(survey) {
	survey.start_availability = Date.parse(survey.start_availability)
	survey.end_availability = Date.parse(survey.end_availability)
      })
      $scope.question = SurveyQuest.get({ surveyId: $routeParams.surveyId, questId: $routeParams.questId });

      if ($routeParams.userId == null) {
        $scope.responses = AllResponses.query({ surveyId: $routeParams.surveyId, questId: $routeParams.questId });
      } else {
        $scope.responses = UserResponses.query({ userId: $routeParams.userId, surveyId: $routeParams.surveyId, questId: $routeParams.questId });
      }
      $scope.responses.$promise.then(function (responses) {
        $scope.users_corrected = {}

        responses.forEach(function(r) {
	  $http({
	    url: "/api/users/" + r.id_user + "/questions/" + $routeParams.questId
	  }).then(function(response) {
            $scope.users_corrected[r.id_user] = response.data
	  })
        })
      });

      $scope.submitCorrection = function() {
        if ($scope.users_corrected[this.response.id_user]) {
          if (this.response.score === undefined)
            this.response.score = $scope.users_corrected[this.response.id_user].score
          if ($scope.users_corrected[this.response.id_user].score_explaination && (!this.response.score_explaination || this.response.score_explaination.indexOf($scope.users_corrected[this.response.id_user].score_explaination) == -1)) {
            if (this.response.score_explaination)
              this.response.score_explaination += '\n' + $scope.users_corrected[this.response.id_user].score_explaination
            else
              this.response.score_explaination = $scope.users_corrected[this.response.id_user].score_explaination
          }
        }

        this.response.id_corrector = $scope.user.id
        if (!this.response.time_scored)
          this.response.time_scored = (new Date()).toISOString()
	this.response.$update()
      }
      $scope.submitCorrections = function() {
	this.responses.forEach(function(response) {
	  response.$update()
	})
      }

      $scope.templates = CorrectionTemplate.query({ surveyId: $routeParams.surveyId, questId: $routeParams.questId }, function (tpls) {
        $scope.template_corrected = {}
        tpls.forEach(function(tpl) {
          $scope.template_corrected[tpl.id] = {}

          CorrectionTemplate.query({ surveyId: $routeParams.surveyId, questId: $routeParams.questId, correctId: tpl.id }, function (cts) {
            cts.forEach(function(ct) {
              $scope.template_corrected[tpl.id][ct.id_user] = ct
            })
          });
        })
      });

      $scope.changeCorrection = function(id_user) {
        var tpl = this.template
        if ($scope.template_corrected[tpl.id] && $scope.template_corrected[tpl.id][id_user]) {
	  $http({
	    url: "/api/users/" + id_user + "/corrections/" + $scope.template_corrected[tpl.id][id_user].id,
	    method: "DELETE"
	  }).then(function(response) {
            $scope.template_corrected[tpl.id][id_user] = false
            $scope.users_corrected[id_user] = response.data
	  })
        } else {
	  $http({
	    url: "/api/users/" + id_user + "/corrections",
	    data: {id_template: tpl.id},
	    method: "POST"
	  }).then(function(response) {
            if ($scope.template_corrected[tpl.id] === undefined)
              $scope.template_corrected[tpl.id] = {}
            $scope.template_corrected[tpl.id][id_user] = { id: response.data.last_id, id_template: tpl.id }
            $scope.users_corrected[id_user] = response.data
	  })
        }
      }

      $scope.deleteTemplate = function() {
        var template = new CorrectionTemplate({
          id: $('#tcid').val() == "" ? null : parseInt($('#tcid').val()),
        })

	template.$remove({ surveyId: $scope.survey.id, questId: $scope.question.id, correctId: template.id }, function() {
          $scope.templates = CorrectionTemplate.query({ surveyId: $scope.survey.id, questId: $scope.question.id });
          angular.element('#correctionTemplateModal').modal('hide')
	});
      }

      $scope.saveTemplate = function() {
        var template = new CorrectionTemplate({
          id: $('#tcid').val() == "" ? null : parseInt($('#tcid').val()),
          id_question: parseInt($('#tcidquestion').val()),
          label: $('#tclabel').val(),
          score: parseInt($('#tcscore').val()),
          score_explaination: $('#tcexplaination').val()
        })

	if (template.id) {
	  template.$update({ surveyId: $scope.survey.id, questId: $scope.question.id, correctId: template.id }, function () {
            $scope.templates = CorrectionTemplate.query({ surveyId: $scope.survey.id, questId: $scope.question.id });
            angular.element('#correctionTemplateModal').modal('hide')
          });
	} else {
	  template.$save({ surveyId: $scope.survey.id, questId: $scope.question.id }, function () {
            $scope.templates = CorrectionTemplate.query({ surveyId: $scope.survey.id, questId: $scope.question.id });
            angular.element('#correctionTemplateModal').modal('hide')
          });
	}
      }
    })

    .controller("QuestionsController", function($scope, SurveyQuest, MyResponse, UserResponses, $http, $routeParams, $location, $rootScope) {
      $rootScope.usersResponses = null;
      $scope.questions = SurveyQuest.query({ surveyId: $scope.survey.id });
      $scope.questions.$promise.then(function (questions) {
        $scope.showSubmit = true;
        var mapQuestions = {};
        questions.forEach(function (question) {
          mapQuestions[question.id] = question;
        });
        $scope.mapQuestions = mapQuestions;
      }, function (response) {
	$scope.addToast({
	  variant: "danger",
	  title: $scope.survey.title,
	  msg: "Une erreur s'est produite lors de l'accès aux questions : <strong>" + (response.data ? response.data.errmsg : "impossible de contacter le serveur") + "</strong>",
	});
	$location.url("surveys/")
      })
      if ($routeParams.userId == null) {
        $scope.myresponses = MyResponse.query({ surveyId: $scope.survey.id });
      } else {
        $scope.myresponses = UserResponses.query({ userId: $routeParams.userId, surveyId: $scope.survey.id });
      }
      $scope.myresponses.$promise.then(function (responses) {
	$scope.questions.$promise.then(function (questions) {
	  var idxquestions = {}
	  questions.forEach(function(question, qid) {
	    idxquestions[question.id] = qid;
	  });

	  responses.forEach(function(response) {
	    if (!questions[idxquestions[response.id_question]].response) {
	      if (response.value) {
		questions[idxquestions[response.id_question]].response = response;
                if (questions[idxquestions[response.id_question]].kind == "text" || questions[idxquestions[response.id_question]].kind == "ucq") {
		  questions[idxquestions[response.id_question]].value = response.value;
                } else if (questions[idxquestions[response.id_question]].kind == "int") {
		  questions[idxquestions[response.id_question]].value = parseInt(response.value);
                } else {
                  response.value.split(",").forEach(function (val) {
                    questions[idxquestions[response.id_question]]["p" + val] = true;
                  })
                }
	      }
	    }
	  })
	});
      });

      $scope.submitAnswers = function() {
	$scope.submitInProgress = true;
	var res = [];
	$scope.questions.forEach(function(q) {
          if (q.kind == "text")
	    res.push({"id_question": q.id, "value": q.value})
          else if (q.kind == "int")
	    res.push({"id_question": q.id, "value": "" + q.value})
          else if (q.kind == "ucq")
	    res.push({"id_question": q.id, "value": q.value})
          else {
            var values = [];
            Object.keys(q).forEach(function (k) {
              if (r = k.match(/^p([0-9]+)$/)) {
                if (q[k])
                  values.push(r[1])
              }
            })
	    res.push({"id_question": q.id, "value": values.join(",")})
          }
	});
        var url = "/api/surveys/" + $scope.survey.id;
        if ($routeParams.userId != null) {
          url = "/api/users/" + $routeParams.userId + "/surveys/" + $scope.survey.id;
        }
	$http({
	  url: url,
	  data: res,
	  method: "POST"
	}).then(function(response) {
	  $scope.submitInProgress = false;
	  $scope.addToast({
	    variant: "success",
	    title: $scope.survey.title,
	    msg: "Vos réponses ont bien étés sauvegardées.",
	  });
	}, function(response) {
	  $scope.submitInProgress = false;
	  $scope.addToast({
	    variant: "danger",
	    title: $scope.survey.title,
	    msg: "Une erreur s'est produite durant l'envoi de vos réponses : " + (response.data ? response.data.errmsg : "impossible de contacter le serveur") + "<br>Veuillez réessayer dans quelques instants.",
	  });
	});
      }

      $scope.saveQuestion = function() {
	this.question.edit = false;
	if (this.question.id) {
	  this.question.$update({ surveyId: $scope.survey.id });
	} else {
	  this.question.$save({ surveyId: $scope.survey.id });
	}
      }

      $scope.addQuestion = function() {
	$scope.questions.push(new SurveyQuest({edit:true, kind: 'text'}))
      }

      $scope.editQuestion = function() {
	this.question.edit = true;
      }

      $scope.deleteQuestion = function() {
	if (this.question.id) {
	  this.question.$remove(function() {
	    $scope.questions = SurveyQuest.query({ surveyId: $scope.survey.id });
	  });
	} else {
	  $scope.questions.splice($scope.questions.indexOf(this.question), 1);
	}
      }
    })

    .controller("ProposalsController", function($scope, QuestProposal) {
      $scope.proposals = QuestProposal.query({ surveyId: $scope.survey.id, questId: $scope.question.id });

      $scope.saveProposal = function() {
	if (this.proposal.id) {
	  this.proposal.$update({ surveyId: $scope.survey.id, questId: $scope.question.id, proposalId: this.proposal.id });
	} else {
	  this.proposal.$save({ surveyId: $scope.survey.id, questId: $scope.question.id });
	}
      }

      $scope.addProposal = function() {
	$scope.proposals.push(new QuestProposal({}))
      }

      $scope.deleteProposal = function() {
	if (this.proposal.id) {
	  this.proposal.$remove(function() {
	    $scope.proposals = QuestProposal.query({ surveyId: $scope.survey.id, questId: $scope.question.id });
	  });
	} else {
	  $scope.proposals.splice($scope.proposals.indexOf(this.proposal), 1);
	}
      }
    })

    .controller("CorrectionsTemplateController", function($scope, CorrectionTemplate) {
      $scope.addTemplate = function() {
	var element = angular.element('#correctionTemplateModal');
        var tpl = new CorrectionTemplate({id_question: $scope.question.id})
        if (element.data('bs.modal'))
          element.data('bs.modal')._config.template = tpl
        element.modal({
          template: tpl
        });
      }

      $scope.editTemplate = function() {
	var element = angular.element('#correctionTemplateModal');
        if (element.data('bs.modal'))
          element.data('bs.modal')._config.template = this.template
        element.modal({
          template: this.template
        });
      }
    })
