//go:build dev
// +build dev

package main

import (
	"flag"
	"net/http"
	"os"
	"path/filepath"
)

var (
	Assets    http.FileSystem
	StaticDir string = "htdocs/"
)

func init() {
	flag.StringVar(&StaticDir, "static", StaticDir, "Directory containing static files")
}

func sanitizeStaticOptions() error {
	StaticDir, _ = filepath.Abs(StaticDir)
	if _, err := os.Stat(StaticDir); os.IsNotExist(err) {
		StaticDir, _ = filepath.Abs(filepath.Join(filepath.Dir(os.Args[0]), "htdocs"))
		if _, err := os.Stat(StaticDir); os.IsNotExist(err) {
			return err
		}
	}
	Assets = http.Dir(StaticDir)
	return nil
}
