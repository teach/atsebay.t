FROM node:21-alpine as nodebuild

WORKDIR /ui

COPY ui/ .

RUN npm install --network-timeout=100000 && \
    npm run build


FROM golang:1-alpine as gobuild

RUN apk add --no-cache go-bindata

WORKDIR /go/src/git.nemunai.re/atsebay.t

COPY *.go go.mod go.sum ./
COPY --from=nodebuild /ui/build ui/build

RUN go get -d -v && \
    go generate -v && \
    go build -v -buildvcs=false -ldflags="-s -w" -o atsebay.t


FROM alpine:3.19

EXPOSE 8081

ENTRYPOINT ["atsebay.t"]

COPY --from=gobuild /go/src/git.nemunai.re/atsebay.t/atsebay.t /usr/bin/atsebay.t