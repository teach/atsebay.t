package main

import (
	"fmt"
	"log"
	"math"
	"net/http"
	"strconv"
	"strings"

	"github.com/drone/drone-go/drone"
	"github.com/gin-gonic/gin"
)

func declareAPIAdminGradationRoutes(router *gin.RouterGroup) {
	router.GET("/gradation_repositories", func(c *gin.Context) {
		client := drone.NewClient(droneEndpoint, droneConfig)
		result, err := client.RepoList()
		if err != nil {
			log.Println("Unable to retrieve the repository list:", err.Error())
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": "Unable to retrieve the repository list."})
			return
		}
		c.JSON(http.StatusOK, result)
	})
	router.POST("/gradation_repositories/sync", func(c *gin.Context) {
		client := drone.NewClient(droneEndpoint, droneConfig)
		result, err := client.RepoListSync()
		if err != nil {
			log.Println("Unable to retrieve the repository list:", err.Error())
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": "Unable to retrieve the repository list."})
			return
		}
		c.JSON(http.StatusOK, result)
	})
}

type TestsWebhook struct {
	Login        string             `json:"login"`
	RepositoryId int                `json:"repository_id"`
	BuildNumber  int                `json:"build_number"`
	UpTo         float64            `json:"upto"`
	Steps        map[string]float64 `json:"steps,omitempty"`
}

func (tw *TestsWebhook) fetchRepoTests(r *Repository) error {
	tmp := strings.Split(r.TestsRef, "/")
	if len(tmp) < 3 {
		return fmt.Errorf("This repository tests reference is not filled properly.")
	}

	work, err := getWork(r.IdWork)
	if err != nil {
		return fmt.Errorf("Unable to retrieve the related work: %w", err)
	}

	client := drone.NewClient(droneEndpoint, droneConfig)
	result, err := client.Build(tmp[0], tmp[1], tw.BuildNumber)
	if err != nil {
		return fmt.Errorf("Unable to find the referenced build (%d): %w", tw.BuildNumber, err)
	}

	if result.Finished > 0 {
		return fmt.Errorf("The test phase is not finished")
	}

	var grade float64
	for _, stage := range result.Stages {
		for _, step := range stage.Steps {
			if g, ok := tw.Steps[fmt.Sprintf("%d", step.Number)]; ok {
				log.Printf("Step %q (%d) in status %q", step.Name, step.Number, step.Status)
				// Give the point if it succeed
				if step.Status == "success" {
					grade += g
				}
				continue
			}

			if g, ok := tw.Steps[step.Name]; ok {
				log.Printf("Step %q (%d) in status %q", step.Name, step.Number, step.Status)
				// Give the point if it succeed
				if step.Status == "success" {
					grade += g
				}
				continue
			}

			logs, err := client.Logs(tmp[0], tmp[1], tw.BuildNumber, stage.Number, step.Number)
			if err != nil {
				log.Printf("Unable to retrieve build logs %s/%s/%d/%d/%d: %s", tmp[0], tmp[1], tw.BuildNumber, stage.Number, step.Number, err.Error())
				continue
			}

			if len(logs) < 2 {
				continue
			}

			line := logs[len(logs)-1]
			if strings.HasPrefix(logs[len(logs)-2].Message, "+ echo grade:") && strings.HasPrefix(line.Message, "grade:") {
				g, err := strconv.ParseFloat(strings.TrimSpace(strings.TrimPrefix(line.Message, "grade:")), 64)
				if err == nil {
					grade += g
				} else {
					log.Println("Unable to parse grade:", err.Error())
				}
			}
		}
	}

	if tw.UpTo != 0 {
		grade = math.Trunc(grade*2000/tw.UpTo) / 100
	}

	work.AddGrade(WorkGrade{
		IdUser: r.IdUser,
		IdWork: work.Id,
		Grade:  grade,
	})

	return nil
}
